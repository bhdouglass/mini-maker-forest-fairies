TEMPLATE = aux
TARGET = MiniMakerFF

RESOURCES += MiniMakerFF.qrc

QML_FILES += $$files(*.qml,true) \
             $$files(*.js,true)

CONF_FILES +=  MiniMakerFF.apparmor \
               MiniMakerFF.png \
               Splash.png

OTHER_FILES += $${CONF_FILES} \
               $${QML_FILES} \
               MiniMakerFF.desktop

#specify where the qml/js files are installed to
qml_files.path = /MiniMakerFF
qml_files.files += $${QML_FILES}

#specify where the config files are installed to
config_files.path = /MiniMakerFF
config_files.files += $${CONF_FILES}

#install the desktop file, a translated version is
#automatically created in the build directory
desktop_file.path = /MiniMakerFF
desktop_file.files = $$OUT_PWD/MiniMakerFF.desktop
desktop_file.CONFIG += no_check_exist

#images files
my_images.path = /MiniMakerFF
my_images.files += images/*

INSTALLS+=config_files qml_files desktop_file my_images

DISTFILES += \
    BackgroundPanel.qml \
    ElementsPanel.qml \
    BackgroundThumb.qml \
    Element.qml \
    ElementBase.qml \
    ElementsModel.qml \
    StartScreen.qml \
    PlayScreen.qml \
    MenuPanel.qml \
    PressButton.qml \
    SnapImage.qml \
    PlayField.qml \
    BinPanel.qml \
    Storage.js \
    ShowScreen.qml \
    ShowSnapDelegate.qml \
    ShowElement.qml \
    DeleteConfirm.qml \
    AboutPage.qml

