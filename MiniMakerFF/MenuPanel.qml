import QtQuick 2.4
import "Storage.js" as Storage

Rectangle {
    id: root

    width: menuPanelVisible ? main_row.width : 0
    color: "#efefef"

    SequentialAnimation {
        id: flash_anim

        PropertyAction { target: flash; properties: "width"; value: main_window.width }

        PropertyAction { target: flash; property: "height"; value: main_window.height }

        PauseAnimation { duration: 200 }

        PropertyAction { target: flash; properties: "width"; value: 0 }

        PropertyAction { target: flash; property: "height"; value: 0 }
    }

    Row {
        id: main_row

        spacing: units.gu(.5)
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }

        PressButton {
            id: home_btn

            width: height
            height: panelHeight
            img_source: "ui/home-btn.svg"
            img_press_source: "ui/home-btn-press.svg"
            img_width: parent.width
            onClicked: {
                menuPanelVisible = false
                isPlayScreen = false
            }
        }

        PressButton {
            id: photo_btn

            visible: menuPanelVisible && play_field.children.length > 0
            width: height
            height: panelHeight
            img_source: "ui/camera-btn.svg"
            img_press_source: "ui/camera-btn-press.svg"
            img_width: parent.width
            ma.onPressed: {
                var stamp = Date.now().toString()
                var backurl = backgroundImg

                flash_anim.start()
                Storage.saveBacks(stamp, backurl)

                for (var i = 0; i < play_field.children.length; i++ ) {
                    var iurl = play_field.children[i].elemImg
                    var ix = play_field.children[i].x
                    var iy = play_field.children[i].y
                    var is = play_field.children[i].scale
                    var iMirror = play_field.children[i].isMirror
                    var iwidth = play_field.children[i].width / units.gu(1)
                    var irot = play_field.children[i].rotation
                    Storage.saveSnaps(stamp, iurl, ix, iy, is, iMirror, iwidth, irot)
                }

                play_field.grabToImage(function(result) {
                    createSnapImage(result.url)
                })
                hasSnaps = true

            }
        }

        PressButton {
            id: reset_btn

            visible: menuPanelVisible && play_field.children.length > 0
            width: height
            height: panelHeight
            img_source: "ui/bin-btn.svg"
            img_press_source: "ui/bin-btn-press.svg"
            img_width: parent.width
            ma.onPressed: {
                for (var i = 0; i < play_field.children.length; i++ ) {
                    play_field.children[i].remove_anim.start()
                    play_field.children[i].destroy(150)
                }
            }
        }
    }

}

