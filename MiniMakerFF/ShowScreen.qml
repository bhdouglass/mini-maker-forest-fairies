import QtQuick 2.4
import Ubuntu.Content 1.3
import "Storage.js" as Storage

Item {
    id: root

    property alias back_listview: backs_listview
    property alias backs_listmodel: backs_listmodel
    property string resultURL: "/home/phablet/.local/share/minimakerff.bhdouglass/shareimg.png"

    // timer to get elements for first item
    Timer {
        id: timer_check

        interval: 30
        running: true
        triggeredOnStart: true
        repeat: !backs_listmodel.get(backs_listview.currentIndex).backtimestamp
        onTriggered: Storage.getSnaps(backs_listmodel.get(backs_listview.currentIndex).backtimestamp)
    }

    Image {
        id: back

        source: "ui/show_back.svg"
        sourceSize.height: parent.height
        anchors.centerIn: parent
    }

    ListView {
        id: backs_listview

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: controls.top
        }
        orientation: ListView.Horizontal
        clip: true
        interactive: false
        highlightFollowsCurrentItem: true
        highlightMoveDuration: 333
        snapMode: ListView.SnapOneItem
        cacheBuffer: root.width

        remove: Transition {NumberAnimation { property: "y"; to: - root.height; duration: 150 }}
        displaced: Transition {NumberAnimation { property: "x"; duration: 333 }}

        model: ListModel {
            id: backs_listmodel
        }

        delegate: Item {
            id: deleg

            property string stamp: backtimestamp

            width: root.width
            height: root.height - panelHeight
            clip: true

            Image {

                source: back_source
                sourceSize.height: root.height - controls.height
                height: sourceSize.height
                fillMode: Image.PreserveAspectCrop
                anchors.centerIn: parent
            }
        }
    }

    MouseArea {
        anchors.fill: parent
    }

    Item {
        id: controls

        width: parent.width
        height: panelHeight
        anchors.bottom: parent.bottom

        Rectangle {
            id: controls_back

            color: "#efefef"
            anchors.fill: parent
        }

        PressButton {
            id: home_btn

            width: height
            height: panelHeight
            img_source: "ui/home-btn.svg"
            img_press_source: "ui/home-btn-press.svg"
            img_width: panelHeight
            onClicked: {
                backs_listmodel.clear()
                isShowScreen = false
            }
        }

        PressButton {
            id: delete_btn

            visible: backs_listmodel.count > 0
            width: height
            height: panelHeight
            anchors {
                left: home_btn.right
                leftMargin: units.gu(4)
            }

            img_source: "ui/bin-btn.svg"
            img_press_source: "ui/bin-btn-press.svg"
            img_width: panelHeight
            onClicked: delete_confirm.visible = true
        }

        PressButton {
            id: scroll_left

            visible: backs_listview.currentIndex > 0
            width: height
            height: panelHeight
            anchors.right: parent.horizontalCenter
            img_source: "ui/scroll-left-btn.svg"
            img_press_source: "ui/scroll-left-btn-press.svg"
            img_width: panelHeight
            onClicked: {
                if (backs_listview.currentIndex > 0) {
                    backs_listview.currentIndex -= 1
                    Storage.getSnaps(backs_listmodel.get(backs_listview.currentIndex).backtimestamp)
                }
            }
        }

        PressButton {
            id: scroll_right

            visible: backs_listview.currentIndex < backs_listmodel.count - 1
            width: height
            height: panelHeight
            anchors.left: parent.horizontalCenter
            img_source: "ui/scroll-left-btn.svg"
            img_press_source: "ui/scroll-left-btn-press.svg"
            img_width: panelHeight
            isImgMirrored: true
            onClicked: {
                if (backs_listview.currentIndex < backs_listmodel.count - 1) {
                    backs_listview.currentIndex += 1
                    Storage.getSnaps(backs_listmodel.get(backs_listview.currentIndex).backtimestamp)
                }
            }
        }

        PressButton {
            id: share

            visible: backs_listmodel.count > 0
            width: height
            height: panelHeight
            anchors.right: parent.right
            img_source: "ui/share-btn.svg"
            img_press_source: "ui/share-btn-press.svg"
            img_width: panelHeight
            onClicked: {
                back_listview.currentItem.grabToImage(function(result) {
                    result.saveToFile(resultURL)
                })
                cpp.visible = true
            }
        }
    }

    DeleteConfirm {
        id: delete_confirm

        visible: false
        anchors.fill: parent
        del_btn.onClicked: {
            Storage.deleteSnaps(backs_listmodel.get(backs_listview.currentIndex).backtimestamp)
            Storage.deleteBacks(backs_listmodel.get(backs_listview.currentIndex).backtimestamp)
            backs_listmodel.remove(backs_listview.currentIndex)
            timer_check.start()
            if (backs_listmodel.count < 1) {hasSnaps = false}
            delete_confirm.visible = false
        }
        onCancel: delete_confirm.visible = false
    }

    ContentItem {
        id: exportItem
    }

    ContentPeerPicker {
        id: cpp

        visible: false
        handler: ContentHandler.Share
        contentType: ContentType.Pictures
        onPeerSelected: {
            var transfer = peer.request();
            var items = new Array();
            exportItem.url = resultURL
            items.push(exportItem);
            transfer.items = items;
            transfer.state = ContentTransfer.Charged;
            cpp.visible = false;
        }
        onCancelPressed: cpp.visible = false;
    }


}

