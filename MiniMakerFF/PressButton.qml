import QtQuick 2.4

Item {
    id: root

    property color b_color: "#efefef"
    property string img_source
    property string img_press_source
    property int img_width
    property bool isImgMirrored: false
    property alias ma: ma

    signal clicked


    Rectangle {
        id: background

        color: b_color
        anchors.fill: parent

        Image {
            id: button_image

            source: img_source
            sourceSize.width: img_width
            anchors.fill: parent
            mirror: isImgMirrored
        }

        MouseArea {
            id: ma

            anchors.fill: parent
            onPressed: button_image.source = img_press_source
            onReleased: button_image.source = img_source
            onClicked: root.clicked()
        }
    }
}

