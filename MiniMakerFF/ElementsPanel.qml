import QtQuick 2.4

Item {
    id: root

    visible: !binPanelVisible

    NumberAnimation {
        id: scroll_left_anim

        target: elements_row
        property: "x"
        to: elements_row.x + 3 * elements.width / 4
        duration: 500
        easing.type: Easing.OutBack
    }

    NumberAnimation {
        id: scroll_right_anim

        target: elements_row
        property: "x"
        to: elements_row.x - 3 * elements.width / 4
        duration: 500
        easing.type: Easing.OutBack
    }

    ElementsModel {
        id: emodel
    }

    Rectangle {
        id: elements

        color: "#efefef"
        height: panelHeight
        anchors {
            left: scroll_left.right
            right: scroll_right.left
        }

        Flickable {
            id: flickable

            interactive: false
            anchors.fill: parent
            contentWidth: elements_row.width
            flickableDirection: Flickable.HorizontalFlick

            Row {
                id: elements_row

                anchors.verticalCenter: parent.verticalCenter

                Repeater {
                    model: emodel
                    ElementBase {
                        baseImg: image
                        widthStart: widthS
                        sMin: scaleMin
                        sMax: scaleMax
                    }
                }
            }
        }
    }

    PressButton {
        id: menu

        width: height
        height: panelHeight
        img_source: "ui/menu-btn.svg"
        img_press_source: "ui/menu-btn-press.svg"
        img_width: panelHeight
        ma.onPressed: {
            menuPanelVisible = !menuPanelVisible
            backPanelVisible = false
        }
    }

    MenuPanel {
        id: menu_panel

        height: panelHeight
        anchors {
            left: menu.right
        }
        Behavior on width {NumberAnimation { easing.type: Easing.OutExpo; duration: 333 }}
    }

    PressButton {
        id: scroll_left

        width: height
        height: panelHeight
        anchors.left: menu_panel.right
        img_source: "ui/scroll-left-btn.svg"
        img_press_source: "ui/scroll-left-btn-press.svg"
        img_width: panelHeight
        ma.onPressed: {
            if (menuPanelVisible) {
                menuPanelVisible = false
            }
            else if (backPanelVisible) {
                backPanelVisible = false
            }
            else if (elements_row.x < 0){
                scroll_left_anim.start()
            }
        }
    }

    PressButton {
        id: scroll_right

        width: height
        height: panelHeight
        anchors.right: back_panel.left
        img_source: "ui/scroll-left-btn.svg"
        img_press_source: "ui/scroll-left-btn-press.svg"
        img_width: panelHeight
        isImgMirrored: true
        ma.onPressed: {
            if (menuPanelVisible) {
                menuPanelVisible = false
            }
            else if (backPanelVisible) {
                backPanelVisible = false
            }
            else if (elements_row.x > - (elements_row.width - elements.width)) {
                scroll_right_anim.start()
            }
        }
    }

    BackgroundPanel {
        id: back_panel

        height: panelHeight
        anchors.right: background_switch.left
        Behavior on width {NumberAnimation { easing.type: Easing.OutExpo; duration: 333 }}
    }

    Rectangle {
        id: background_switch

        color: "#efefef"
        width: 3 * height / 2
        height: panelHeight
        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
        }

        Image {
            id: back_switch_img

            source: "ui/back-switch-btn.svg"
            sourceSize.height: panelHeight
            anchors.fill: parent

        }

        Rectangle {
            id: color_rct

            anchors {
                top: parent.top
                topMargin: parent.height / 6
                horizontalCenter: parent.horizontalCenter
            }
            width: 7 * height / 4
            height: parent.height / 2
            radius: 4

            Image {
                id: back_img

                source: backgroundImg
                sourceSize.height: parent.height
                anchors.centerIn: parent
                width: parent.width
                fillMode: Image.PreserveAspectCrop
            }
        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                backPanelVisible = !backPanelVisible
                menuPanelVisible = false
                back_switch_img.source = "ui/back-switch-btn-press.svg"
                color_rct.anchors.topMargin = background_switch.height / 4.5
            }
            onReleased: {
                color_rct.anchors.topMargin = background_switch.height / 6
                back_switch_img.source = "ui/back-switch-btn.svg"
            }
        }
    }

}

