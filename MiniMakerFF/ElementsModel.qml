import QtQuick 2.4

ListModel {
    ListElement {
        image: "dom1"
        widthS: 20
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "dom2"
        widthS: 20
        scaleMin: .2
        scaleMax: 2.2
    }
    ListElement {
        image: "dom3"
        widthS: 20
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "dom4"
        widthS: 20
        scaleMin: .4
        scaleMax: 2.5
    }
    ListElement {
        image: "wrozka1"
        widthS: 18
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "wrozka2"
        widthS: 18
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "wrozka3"
        widthS: 20
        scaleMin: .2
        scaleMax: 3.2
    }
    ListElement {
        image: "wrozka4"
        widthS: 20
        scaleMin: .4
        scaleMax: 3.2
    }
    ListElement {
        image: "wrozka5"
        widthS: 20
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "wrozka6"
        widthS: 20
        scaleMin: .6
        scaleMax: 4
    }
    ListElement {
        image: "zabka1"
        widthS: 14
        scaleMin: .2
        scaleMax: 4
    }
    ListElement {
        image: "slimak1"
        widthS: 14
        scaleMin: .2
        scaleMax: 4
    }
    ListElement {
        image: "slimak2"
        widthS: 12
        scaleMin: .2
        scaleMax: 4
    }
    ListElement {
        image: "robak1"
        widthS: 10
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "robak2"
        widthS: 12
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "motyl1"
        widthS: 10
        scaleMin: .2
        scaleMax: 5
    }
    ListElement {
        image: "motyl2"
        widthS: 10
        scaleMin: .2
        scaleMax: 5
    }
    ListElement {
        image: "motyl3"
        widthS: 10
        scaleMin: .4
        scaleMax: 5
    }
    ListElement {
        image: "pszczola1"
        widthS: 10
        scaleMin: .4
        scaleMax: 5
    }
    ListElement {
        image: "pszczola2"
        widthS: 8
        scaleMin: .4
        scaleMax: 5
    }
    ListElement {
        image: "biedronka1"
        widthS: 9
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "biedronka2"
        widthS: 8
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "biedronka3"
        widthS: 10
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "truskawka1"
        widthS: 9
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "truskawka2"
        widthS: 12
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "jagoda"
        widthS: 12
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "jezyna"
        widthS: 10
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "grzybek1"
        widthS: 12
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "grzybek2"
        widthS: 10
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "grzybek3"
        widthS: 12
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "kwiat1"
        widthS: 10
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "kwiat2"
        widthS: 12
        scaleMin: .2
        scaleMax: 2
    }
    ListElement {
        image: "kwiat3"
        widthS: 10
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "kwiat4"
        widthS: 10
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "kwiat5"
        widthS: 12
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "kwiat6"
        widthS: 10
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "kwiat7"
        widthS: 10
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "kwiat8"
        widthS: 14
        scaleMin: .2
        scaleMax: 4
    }
    ListElement {
        image: "kwiat9"
        widthS: 13
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "kwiat10"
        widthS: 12
        scaleMin: .2
        scaleMax: 4
    }
}

