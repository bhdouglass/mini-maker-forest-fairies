import QtQuick 2.4

Item {
    id: root

    property string elemImg
    property double widthStart
    property bool isMirror

    visible: isShowScreen

    width: units.gu(widthStart)
    height: elem_image.height

    Image {
        id: elem_image

        source: "stickers/" + elemImg + ".svg"
        anchors.centerIn: parent
        sourceSize.height: (main_window.height - panelHeight) / 2
        width: parent.width
        fillMode: Image.PreserveAspectFit
        mirror: isMirror
    }

}

