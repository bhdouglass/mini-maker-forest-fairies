import QtQuick 2.4
import Ubuntu.Components 1.3

Rectangle {
    id: root

    color: "#efefef"

    Image {
        id: post_mm

        source: "ui/mini-maker.svg"
        anchors{
            top: parent.top
            topMargin: units.gu(4)
            horizontalCenter: parent.horizontalCenter
        }
        sourceSize.width: parent.height / 5
    }

    Label {
        id: title

        text: "Forest Fairies \n v 1.1"
        anchors {
            top: post_mm.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: info

        text: "made by Michał Prędotka and kids\nlicensed under GPL v3"
        anchors {
            top: title.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: www

        text: "website: <a href='https://launchpad.net/mini-maker'>https://launchpad.net/mini-maker</a>"
        anchors {
            top: info.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
        onLinkActivated: Qt.openUrlExternally(link)
    }

    Label {
        id: contact

        text: "contact: <a href='mailto:mivoligo@gmail.com'>mivoligo@gmail.com</a>"
        anchors {
            top: www.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
        onLinkActivated: Qt.openUrlExternally(link)
    }

    PressButton {
        id: home_btn

        width: height
        height: panelHeight
        img_source: "ui/home-btn.svg"
        img_press_source: "ui/home-btn-press.svg"
        img_width: panelHeight
        anchors {
            bottom: parent.bottom
            left: parent.left
        }

        onClicked: isAboutScreen = false
    }

}

