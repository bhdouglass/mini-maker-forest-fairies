import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import "Storage.js" as Storage

Window {
    id: main_window

    property bool isPlayScreen: false
    property bool isShowScreen: false
    property bool isAboutScreen: false
    property string backgroundImg: "backs/background1.svg"
    property bool menuPanelVisible: false
    property bool backPanelVisible: false
    property bool binPanelVisible: false
    property bool hasSnaps: false
    property bool inBinArea: false
    property int panelHeight: Math.min(main_window.height / 8, units.gu(15))

    function createElementObjects(x, y, img, widthStart, sMin, sMax)
    {
        var component = Qt.createComponent("Element.qml");
        var element = component.createObject(play_field, {"x": x, "y": y, "elemImg": img, "widthStart": widthStart, "elemMinScale": sMin, "elemMaxScale": sMax});

        if (element == null) {
            // Error Handling
            console.log("Error creating object in createElementObjects");
        }
    }

    function recreateElementObjects(iurl, x, y, scale, mirror, width, rotation)
    {
        var component = Qt.createComponent("ShowElement.qml");
        var element = component.createObject(screen_loader.item.back_listview.currentItem, {"elemImg": iurl, "x": x, "y": y, "scale": scale, "isMirror": mirror, "widthStart": width, "rotation": rotation });

        if (element == null) {
            // Error Handling
            console.log("Error creating object in recreateElementObjects");
        }
    }

    function createSnapImage(imgSource)
    {
        var component = Qt.createComponent("SnapImage.qml")
        var newSnap = component.createObject(main_window, {"img_source": imgSource,"img_width": play_field.width, "img_height": play_field.height})

        if (element == null) {
            // Error Handling
            console.log("Error creating object in createSnapImage");
        }
    }

    visibility: Window.FullScreen
    title: "Mini Maker"
    width: units.gu(100)
    height: units.gu(60)
    minimumWidth: units.gu(60)
    minimumHeight: units.gu(40)

    Component.onCompleted: {
        if (Storage.checkIfDBVersionTableExists() == "not_exist"){
            console.log("table not exists")
            //create table
            Storage.createDBVersionTable();
            //add value to table
            Storage.saveDBVersion(1);
            // Add column for rotation to snaps table
            if (Storage.checkIfSnapsTableExists() == "table_exist"){
                Storage.addRotationColumnToTable()
            }
        }
        Storage.createSnapsTable()
        Storage.createBacksTable()
    }

    Settings {
        id: settings

        property alias gotSnaps: main_window.hasSnaps
        property alias backgroundImg: main_window.backgroundImg
    }

    MainView {
        applicationName: "minimakerff.bhdouglass"
        anchors.fill: parent

        MouseArea {
            enabled: isPlayScreen
            anchors.fill: parent
            anchors.topMargin: panelHeight
            onClicked: {
                menuPanelVisible = false
                backPanelVisible = false
            }
        }

        Loader {
            active: binPanelVisible
            width: parent.width
            height: panelHeight
            source: "BinPanel.qml"
        }

        Image {
            id: start_screen_background

            scale: 2
            visible: !isPlayScreen && !isAboutScreen
            source: "ui/start-back.svg"
            sourceSize.height: parent.height
            height: parent.height
            anchors {
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter
            }
            Component.onCompleted: NumberAnimation {target: start_screen_background; property: "scale"; easing.type: Easing.OutCubic; to: 1; duration: 1000}
        }

        Component {
            id: play_background_comp

            Image {
                source: backgroundImg
                sourceSize.height: main_window.height - panelHeight
                fillMode: Image.PreserveAspectCrop
            }
        }

        Loader {
            id: play_back_loader

            active: isPlayScreen
            anchors {
                fill: parent
                topMargin: panelHeight
            }
            sourceComponent: play_background_comp
        }

        Item {
            id: play_field

            visible: isPlayScreen
            anchors.fill: parent
            anchors.topMargin: panelHeight
        }

        Loader {
            id: element_panel_loader

            active: isPlayScreen
            width: parent.width
            height: panelHeight
            source: "ElementsPanel.qml"
        }

        Loader {
            id: screen_loader

            anchors.fill: parent
            source: {
                if(isShowScreen) {
                    "ShowScreen.qml"
                }
                else if (isAboutScreen) {
                    "AboutPage.qml"
                }
                else if (!isPlayScreen) {
                    "StartScreen.qml"
                }
                else {""}
            }
        }

        Rectangle {
            id: flash

            anchors.centerIn: parent
            color: "white"
            width: 0
            height: 0
        }
    }
}

