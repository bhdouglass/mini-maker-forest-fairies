import QtQuick 2.4
import "Storage.js" as Storage

Item {
    id: root

    scale: 0
    transformOrigin: Item.Bottom
    Component.onCompleted: NumberAnimation {target: root; property: "scale"; easing.type: Easing.OutCubic; duration: 1000; to: 1}

    Image {
        id: post_back

        source: "ui/post-back.svg"
//        scale: .5
        anchors {
            bottom: parent.bottom
//            bottomMargin: - height
            horizontalCenter: parent.horizontalCenter
        }
        sourceSize.width: Math.min(parent.width, parent.height - units.gu(4))

//        Component.onCompleted: NumberAnimation {target: post_back; property: "scale"; easing.type: Easing.OutCubic; duration: 1000; to: 1}

    }

    Image {
        id: post_mm

        source: "ui/mm-post.svg"
        anchors{
            top: post_back.top
            horizontalCenter: post_back.horizontalCenter
        }
        sourceSize.width: post_back.width

        MouseArea {
            width: parent.width / 4
            height: width
            anchors.centerIn: parent
            onClicked: isAboutScreen = true
        }
    }

    Image {
        id: post_ff

        source: "ui/ff-post.svg"
        anchors {
            top: post_mm.bottom
            horizontalCenter: post_back.horizontalCenter
        }
        sourceSize.width: post_back.width

        MouseArea {
            width: parent.width / 2
            height: parent.height
            anchors.right: parent.right
            onClicked: isPlayScreen = true
        }
    }

    Image {
        id: post_show

        source: main_window.hasSnaps ? "ui/show-post.svg"
                         : "ui/show-empty-post.svg"
        anchors {
            top: post_ff.bottom
            horizontalCenter: post_back.horizontalCenter
        }
        sourceSize.width: post_back.width

        MouseArea {
            enabled: hasSnaps
            width: parent.width / 2
            height: parent.height
            anchors.left: parent.left
            onClicked: {
                isShowScreen = true
                Storage.getBacks(screen_loader.item.backs_listmodel)
            }
        }
    }

    Image {
        id: post_bird

        source: "ui/bird-post.svg"
        sourceSize.height: post_show.height / 2
        anchors {
            bottom: post_show.top
            bottomMargin: - height / 20
            horizontalCenter: parent.horizontalCenter
            horizontalCenterOffset: - post_show.width / 4
        }
        mirror: hasSnaps
    }

}

