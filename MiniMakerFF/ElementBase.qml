import QtQuick 2.4

Item {
    property string baseImg
    property int oldX
    property int oldY
    property double widthStart
    property double sMin
    property double sMax


    width: image_base.width + units.gu(2)
    height: panelHeight - units.gu(2)

    Image {
        id: image_base

        source: "stickers/" + baseImg + ".svg"
        sourceSize.height: parent.height
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        drag.target: parent
        onPressed: {
            oldX = parent.x
            oldY = parent.y
            menuPanelVisible = false
            backPanelVisible = false
        }
        onReleased: {
            if (parent.y > panelHeight) {
                var point = parent.mapToItem(play_field, mouseX, mouseY);
                var halfWidth = units.gu(8) / 2;
                createElementObjects(point.x - halfWidth, point.y - halfWidth, baseImg, 8, 0.2, 5);
            }

            parent.x = oldX
            parent.y = oldY
        }
    }
}
