import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    id: root

    property string elemImg
    property double widthStart
    property double elemMinScale
    property double elemMaxScale
    property bool isMirror: false
    property alias remove_anim: remove_anim

    visible: isPlayScreen
    opacity: inBinArea && elem_ma.drag.active ? .3 : 1
    width: units.gu(widthStart)
    height: elem_image.height
    onYChanged: if (elem_ma.drag.active) binPanelVisible = true

    Timer {
        id: hide_controls_timer

        interval: 4000
        onTriggered: gran_controls.visible = false
    }

    NumberAnimation {
        id: remove_anim

        target: root
        to: - units.gu(2)
        property: "y"
        duration: 150
    }

    Image {
        id: elem_image

        source: "stickers/" + elemImg + ".svg"
        anchors.centerIn: parent
        sourceSize.height: (main_window.height - panelHeight) / 2
        width: parent.width
        fillMode: Image.PreserveAspectFit
        mirror: isMirror

        PinchArea {
            id: pinch_area

            anchors.fill: parent
            pinch.target: root
            //        pinch.minimumRotation: -180
            //        pinch.maximumRotation: 180
            pinch.minimumScale: elemMinScale
            pinch.maximumScale: elemMaxScale
            onPinchFinished: {
                pinch_area.width = Math.max(root.width, units.gu(20))
                pinch_area.height = Math.max(root.height, units.gu(20))
            }
            MouseArea {
                id: elem_ma
                Drag.active: drag.active
                Drag.hotSpot.x: mouseX
                Drag.hotSpot.y: mouseY
                anchors.fill: parent
                drag.target: root
                propagateComposedEvents: true
                onPressed: {
                    menuPanelVisible = false
                    backPanelVisible = false
                }
                onReleased: {
                    binPanelVisible = false

                    if (inBinArea) {
                        root.destroy()
                    }
                }
                onDoubleClicked: if(!gran_controls.visible) isMirror = !isMirror
                onPressAndHold: {
                    gran_controls.visible = true
                    hide_controls_timer.restart()
                }
                onWheel: root.scale += root.scale * wheel.angleDelta.y / 1000
            }
        }

    }

    Rectangle {
        id: gran_controls

        visible: false
        color: "#80585858"
        scale: 1 / root.scale
        width: units.gu(16)
        height: width
        radius: height / 2
        anchors.centerIn: parent
        rotation: 0 - root.rotation

        Rectangle {
            id: grow

            visible: root.scale < elemMaxScale
            color: "#fff"
            width: parent.width / 4
            height: width
            radius: height / 2
            anchors {
                top: parent.top
                topMargin: - parent.width / 8
                horizontalCenter: parent.horizontalCenter
            }
            Icon {
                color: "#222"
                name: "add"
                anchors.fill: parent
                anchors.margins: units.dp(4)
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hide_controls_timer.restart()
                    if(root.scale < elemMaxScale) root.scale = root.scale + .1
                }
            }
        }

        Rectangle {
            id: shrink

            visible: root.scale > elemMinScale
            color: "#fff"
            width: parent.width / 4
            height: width
            radius: height / 2
            anchors {
                bottom: parent.bottom
                bottomMargin: - parent.width / 8
                horizontalCenter: parent.horizontalCenter
            }
            Icon {
                color: "#222"
                name: "remove"
                anchors.fill: parent
                anchors.margins: units.dp(4)
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hide_controls_timer.restart()
                    if(root.scale > elemMinScale) root.scale = root.scale - .1
                }
            }
        }

        Rectangle {
            id: rotate_left

            color: "#fff"
            width: parent.width / 4
            height: width
            radius: height / 2
            anchors {
                left: parent.left
                leftMargin: - parent.width / 8
                verticalCenter: parent.verticalCenter
            }
            Icon {
                color: "#222"
                name: "rotate-left"
                anchors.fill: parent
                anchors.margins: units.dp(4)
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hide_controls_timer.restart()
                    root.rotation = root.rotation - 5
                }
            }
        }

        Rectangle {
            id: rotate_right

            color: "#fff"
            width: parent.width / 4
            height: width
            radius: height / 2
            anchors {
                right: parent.right
                rightMargin: - parent.width / 8
                verticalCenter: parent.verticalCenter
            }
            Icon {
                color: "#222"
                name: "rotate-right"
                anchors.fill: parent
                anchors.margins: units.dp(4)
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hide_controls_timer.restart()
                    root.rotation = root.rotation + 5
                }
            }
        }

        InverseMouseArea {
            anchors.fill: parent
            anchors.margins: - parent.width / 4
            onPressed: gran_controls.visible = false
        }
    }

}

