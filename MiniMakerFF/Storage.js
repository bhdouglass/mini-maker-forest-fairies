
.import QtQuick.LocalStorage 2.0 as Sql

function getDatabase() {
    return Sql.LocalStorage.openDatabaseSync("MiniMakerFF_1", "1.0", "minimakerff", 1000000);
}

// Create special table for DB version number
function createDBVersionTable()
{
    var db = getDatabase();
    db.transaction(
                function(tx) {
                    tx.executeSql('CREATE TABLE IF NOT EXISTS dbversion' +
                                  '(dbversionvalue REAL)');
                }
                );
}

// Check if DB value exist
function checkIfDBVersionExist(db_version)
{
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM dbversion WHERE dbversionvalue = ?',
                                           [db_version]);
                    if (rs.rows.length > 0 ) {
                        result = "true";
                    }
                    else {
                        result = "false";
                    }
                }
                );
    return result
}

function checkIfDBVersionTableExists()
{
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT name FROM sqlite_master WHERE name="dbversion" AND type="table"');
                    if (rs.rows.length > 0 ) {
                        result = "exist";
                    }
                    else {
                        result = "not_exist";
                    }
                }
                );
    return result
}

// Save DB version to DB
function saveDBVersion(db_version)
{
    var db = getDatabase();
    var result = "";
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('INSERT INTO dbversion VALUES(?)',
                                           [db_version]);
                    if (rs.rowsAffected > 0) {
                        result = "DB version saved as: " + db_version;
                    } else {
                        result = "Error with saving DB version: " + db_version;
                    }
                }
                );
    console.log(result)
}

// Update DB version
function updateDBVersion(db_version)
{
    var db = getDatabase();
    var result = "";
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('UPDATE dbversion '+
                                  'SET dbversionvalue = ? ',
                                  [db_version]);
                    if (rs.rowsAffected > 0) {
                        result = "DB version saved as: " + db_version;
                    } else {
                        result = "Error with saving DB version: " + db_version;
                    }
                }
                );
    console.log(result)
}

// Check if snaps table already exists
function checkIfSnapsTableExists()
{
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT name FROM sqlite_master WHERE name="snaps" AND type="table"');
                    if (rs.rows.length > 0 ) {
                        result = "table_exist";
                    }
                    else {
                        result = "table_not_exist";
                    }
                }
                );
    return result
}

// Add column to a table for rotation
function addRotationColumnToTable(){
    var db = getDatabase();
    db.transaction(
                function(tx){
                    tx.executeSql('ALTER TABLE snaps ADD COLUMN imgRotation REAL')
                }
                );
}

// Create backgrounds table
function createBacksTable() {
    var db = getDatabase();
    db.transaction(
                function(tx){
                    tx.executeSql('CREATE TABLE IF NOT EXISTS backs' +
                                  '(tStamp TEXT,' +
                                  'backUrl TEXT)')
                }
                );
}

// Save backgrounds
function saveBacks(tstamp, bUrl) {
    var db = getDatabase();
    var result = "";
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('INSERT INTO backs VALUES(?, ?)',
                                           [tstamp, bUrl]);
                }
                );
}

// Get backgrounds
function getBacks(backlistmodel) {
    var db = getDatabase();
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM backs');

                    var tstamp = "";
                    var bUrl = "";
                    for(var i = rs.rows.length - 1; i >= 0 ; i--) {
                        tstamp = rs.rows.item(i).tStamp;
                        bUrl = rs.rows.item(i).backUrl;
                        backlistmodel.append({"backtimestamp": tstamp, "back_source": bUrl});
                    }
                }
                );
}

// Delete backgrounds
function deleteBacks(backtstamp) {
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('DELETE FROM backs WHERE tStamp = ?',[backtstamp]);
                }
                );
}

// Create snaps table
function createSnapsTable() {
    var db = getDatabase();
    db.transaction(
                function(tx){
                    tx.executeSql('CREATE TABLE IF NOT EXISTS snaps' +
                                  '(tStamp TEXT,' +
                                  'imgUrl TEXT,' +
                                  'imgX REAL,' +
                                  'imgY REAL,' +
                                  'imgScale REAL,' +
                                  'imgMirror BOOLEAN,' +
                                  'imgWidth REAL,' +
                                  'imgRotation REAL)')
                }
                );
}

// Save snaps
function saveSnaps(tstamp, iUrl, iX, iY, iS, iM, iW, iR) {
    var db = getDatabase();
    var result = "";
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('INSERT INTO snaps VALUES(?, ?, ?, ?, ?, ?, ?, ?)',
                                           [tstamp, iUrl, iX, iY, iS, iM, iW, iR]);
                }
                );
}

// Get snaps
function getSnaps(snaptstamp) {
    var db = getDatabase();
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM snaps WHERE tStamp = ?',[snaptstamp]);
                    var iUrl = "";
                    var iX = "";
                    var iY = "";
                    var iS = "";
                    var iM = "";
                    var iW = "";
                    var iR = "";
                    for(var i = 0; i < rs.rows.length; i++) {
                        iUrl = rs.rows.item(i).imgUrl;
                        iX = rs.rows.item(i).imgX;
                        iY = rs.rows.item(i).imgY;
                        iS = rs.rows.item(i).imgScale;
                        iM = rs.rows.item(i).imgMirror;
                        iW = rs.rows.item(i).imgWidth;
                        iR = rs.rows.item(i).imgRotation;
                        recreateElementObjects(iUrl, iX, iY, iS, iM, iW, iR)
                    }
                }
                );
}

// Delete snaps
function deleteSnaps(snaptstamp) {
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('DELETE FROM snaps WHERE tStamp = ?',[snaptstamp]);
                }
                );
}
